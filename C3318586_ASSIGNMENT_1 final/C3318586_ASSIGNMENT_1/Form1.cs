﻿/* 
 * @(#)fractal.java - Fractal Program Mandelbrot Set 
 * www.eckhard-roessel.de
 * Copyright (c) Eckhard Roessel. All Rights Reserved.
 * 06/30/2000 - 03/29/2000
 */
//Converted from Java to C# by Jake Hawkin - Final on 7/11/2014
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C3318586_ASSIGNMENT_1
{
    public partial class Form1 : Form
    {
        public Form1() //Form 1 is loaded. Initializes code already created by VB
        {
            InitializeComponent(); //This is created by VB and makes a connection between the design objects and code
            Save_Image_Button.Text = "Save Current Image"; //Sets the text of a button upon loading of the form
            Fractal_Refresh.Hide();//This hides the restart button upon loading of the form.
        }

        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye; //Image locations and click locations
        private static double xstart, ystart, xende, yende, xzoom, yzoom; //zoom variables
        private static bool action, rectangle; //Boolean variables controlling the program
        private static float xy;
        private Image picture; //Main bitmap assigned to g1
        private Bitmap updatePicture; //Bitmap for the zoom box
        private Graphics g1, g2; //g1 is main graphics with fractal drawn to it. g2 is graphics for zoom box
        Rectangle zoomRect; //Creating the rectangle for a zoom box
        Pen mandelPen = new Pen(Color.Black); //Pen for drawing the mandel
        Pen updatePen = new Pen(Color.White, 2); //Pen for drawing the zoombox
        private static bool redyes, greenyes, blueyes, yellowyes, pinkyes, lightblueyes;
        
        

        private void Fractal_Start_Click(object sender, EventArgs e)//This is an on click method which starts the fractal
        {
            init(); //Run initiate method which runs initvalues
            start(); //Run start method which runs the mandelbrot method
            label1.Hide();//When the start button is clicked the welcome text is hidden
            Save_Image_Button.Text = "Save Current Image"; //When clicked this sets the save button text back to its original
            Fractal_Start.Hide();//When the start button is clicked it hides itself so that you cannot click it again
            Fractal_Refresh.Show();//When start is clicked
        }

        private void Fractal_Refresh_Click(object sender, EventArgs e)//This is a on click method that restarts the program
        {
            init(); //Run initiate method which runs initvalues
            start(); //Run start method which runs the mandelbrot method
            Save_Image_Button.Text = "Save Current Image"; //This resets the text of the save image button
        }
        
        private void Save_Image_Button_Click(object sender, EventArgs e)//This on click method saves the current image on the screen
        {
            pictureBox1.Image.Save(@"F:\ASEA\C3318586_ASSIGNMENT_1\imagesave.jpeg", ImageFormat.Jpeg);//This saves the current image of the program to file. It contains the picture format
            Save_Image_Button.Text = "Image Saved!";//This sets the button text to tell the user the image has been saved
        }

        private void Fractal_Exit_Click(object sender, EventArgs e)//This button exits the program
        {
            Application.Exit();//This simply exits the application
        }
        
        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult message = MessageBox.Show(" 1. Click Start to start the Fractal Program\n 2. Exit will exit the Program\n 3. When the program has started Restart will appear. This restarts the Program\n 4. To zoom in on the Fractal, click and drag a box where you want to zoom\n 5. To save a current image of the fractal Click the Save Image Button ", "Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        private void Red_Button_Click(object sender, EventArgs e)
        {
            redyes = true;
            mandelbrot();
            greenyes = false;
            blueyes = false;
            yellowyes = false;
            pinkyes = false;
            lightblueyes = false;
        }
        
        private void Green_Button_Click(object sender, EventArgs e)
        {
            greenyes = true;
            mandelbrot();
            redyes = false;
            blueyes = false;
            yellowyes = false;
            pinkyes = false;
            lightblueyes = false;
        }

        private void Blue_Button_Click(object sender, EventArgs e)
        {
            blueyes = true;
            mandelbrot();
            redyes = false;
            greenyes = false;
            yellowyes = false;
            pinkyes = false;
            lightblueyes = false;
        }

        private void Yellow_Button_Click(object sender, EventArgs e)
        {
            yellowyes = true;
            mandelbrot();
            redyes = false;
            greenyes = false;
            blueyes = false;
            pinkyes = false;
            lightblueyes = false;
        }

        private void Pink_Button_Click(object sender, EventArgs e)
        {
            pinkyes = true;
            mandelbrot();
            redyes = false;
            greenyes = false;
            blueyes = false;
            yellowyes = false;
            lightblueyes = false;
        }

        private void LightBlue_Button_Click(object sender, EventArgs e)
        {
            lightblueyes = true;
            mandelbrot();
            redyes = false;
            greenyes = false;
            blueyes = false;
            yellowyes = false;
            pinkyes = false;
        }

        private void Save_State_Click(object sender, EventArgs e)
        {
            TextWriter savestate = new StreamWriter(@"F:\ASEA\C3318586_ASSIGNMENT_1\savestate.txt");
            savestate.WriteLine(xstart);
            savestate.WriteLine(ystart);
            savestate.WriteLine(xende);
            savestate.WriteLine(yende);
            savestate.WriteLine(xzoom);
            savestate.WriteLine(yzoom);
            savestate.Close();
        }

        private void Load_State_Click(object sender, EventArgs e)
        {
            TextReader loadstate = new StreamReader(@"F:\ASEA\C3318586_ASSIGNMENT_1\savestate.txt");
            string xstartString = loadstate.ReadLine();
            string ystartString = loadstate.ReadLine();
            string xendeString = loadstate.ReadLine();
            string yendeString = loadstate.ReadLine();
            string xzoomString = loadstate.ReadLine();
            string yzoomString = loadstate.ReadLine();
            xstart = Convert.ToDouble(xstartString);
            ystart = Convert.ToDouble(ystartString);
            xende = Convert.ToDouble(xendeString);
            yende = Convert.ToDouble(yendeString);
            xzoom = Convert.ToDouble(xzoomString);
            yzoom = Convert.ToDouble(yzoomString);
            loadStateStart();
            loadstate.Close();
        }

        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;

            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public float H
            {
                get { return h; }
            }

            public float S
            {
                get { return s; }
            }

            public float B
            {
                get { return b; }
            }

            public int A
            {
                get { return a; }
            }

            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }

            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;

                    float h = hsbColor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                    if (redyes == true)
                    {
                        if (h < 60f)
                        {
                            r = max;
                            g = h * dif / 60f + min;
                            b = min;
                        }
                        else
                        {
                            r = 255;
                            g = 0;
                            b = 0;
                        }
                    }
                    if (greenyes == true)
                    {
                        if (h < 60f)
                        {
                            r = max;
                            g = h * dif / 60f + min;
                            b = min;
                        }
                        else
                        {
                            r = 0;
                            g = 255;
                            b = 0;
                        }
                    }
                    if (blueyes == true)
                    {
                        if (h < 60f)
                        {
                            r = max;
                            g = h * dif / 60f + min;
                            b = min;
                        }
                        else
                        {
                            r = 0;
                            g = 0;
                            b = 255;
                        }
                    }
                    if (yellowyes == true)
                    {
                        if (h < 60f)
                        {
                            r = max;
                            g = h * dif / 60f + min;
                            b = min;
                        }
                        else
                        {
                            r = 255;
                            g = 255;
                            b = 0;
                        }
                    }
                    if (pinkyes == true)
                    {
                        if (h < 60f)
                        {
                            r = max;
                            g = h * dif / 60f + min;
                            b = min;
                        }
                        else
                        {
                            r = 255;
                            g = 0;
                            b = 255;
                        }
                    }
                    if (lightblueyes == true)
                    {
                        if (h < 60f)
                        {
                            r = max;
                            g = h * dif / 60f + min;
                            b = min;
                        }
                        else
                        {
                            r = 0;
                            g = 255;
                            b = 255;
                        }
                    }
                }

                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

        }

        public void init() // all instances will be prepared
        {
            x1 = this.pictureBox1.Width; //Sets the x1 to the width of the picture box
            y1 = this.pictureBox1.Height; //Sets the y1 to the height
            xy = (float)x1 / (float)y1; //
            picture = new Bitmap(this.pictureBox1.Width, this.pictureBox1.Height); //Creates the bitmap
            g1 = Graphics.FromImage(picture); //Creates a graphics context for the bitmap
            pictureBox1.Image = picture; //Sets the picture box image to the bitmap
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        public void start()//Starts the program and calls the mandelbrot method 
        {
            action = false;
            rectangle = false;
            redyes = false;
            greenyes = false;
            blueyes = false;
            yellowyes = false;
            pinkyes = false;
            lightblueyes = false;
            initvalues();//This method initiates values 
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();//This method draws the fractal
        }

        public void loadStateStart()
        {
            action = false;
            rectangle = false;
            init();
            mandelbrot();
            label1.Hide();//When the start button is clicked the welcome text is hidden
            Save_Image_Button.Text = "Save Current Image"; //When clicked this sets the save button text back to its original
            Fractal_Start.Hide();//When the start button is clicked it hides itself so that you cannot click it again
            Fractal_Refresh.Show();//When start is clicked
        }
        
        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }
        
        private void mandelbrot() // calculate all points
        {
            int x, y; //Creates the x and y pixel variables for the for loops
            float h, b, alt = 0.0f;
            action = false;
            for (x = 0; x < x1; x += 2)//For loop to run through each x pixel
            {
                for (y = 0; y < y1; y++)//For loop to run through each y pixel
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightness
                        HSBColor hsbcolor = new HSBColor(h * 255, 0.8f * 255, b * 255);//Creates a new hsb colour. Each variable passed in is times by 255. It is black otherwise.
                        mandelPen = new Pen(hsbcolor.Color);//Setting the colour of the pen
                        alt = h;
                    }
                    g1.DrawLine(mandelPen, x, y, x + 1, y);//Draws a line 1 pixel in length and continues until its has coloured the whole bitmap
                }
            }
            action = true;
            this.Refresh();
        }     
        
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)//This is called when the mouse is clicked down
        {
            this.Refresh();
            rectangle = true;//The rectangle is set to true which activates code within the picture box paint method
            if (action)
            {
                xs = e.X;//This gets the x position of the mouse
                ys = e.Y;//This gets the y position of the mouse
                zoomRect = new Rectangle(xs, ys, 0, 0);//This creates the rectangle and sets the beginning postion and the mouse x and y position
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)//This is called when the mouse is moved
        {
            if (action)//This sets the action to true
            {
                xe = e.X;//This gets the mouse x position while the mouse is moved
                ye = e.Y;//This gets the mouse y position white the mouse is moved
                if (e.Button == MouseButtons.Left)//This detects whether the left mouse button is clicked
                {
                    this.Cursor = Cursors.Cross;
                    if (xs < xe)//This is taken from the update method. It determines the direction of the mouse drag
                    {
                        if (ys < ye) zoomRect = new Rectangle(xs, ys, (xe - xs), (ye - ys));//This creates the rectangle and sets the positions relative to the mouse
                        else zoomRect = new Rectangle(xs, ye, (xe - xs), (ys - ye));
                    }
                    else
                    {
                        if (ys < ye) zoomRect = new Rectangle(xe, ys, (xs - xe), (ye - ys));
                        else zoomRect = new Rectangle(xe, ye, (xs - xe), (ys - ye));
                    }
                }
                this.Refresh();//This refreshes the form allowing the box to be drawn
            }
        }
        
        
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)//This is called when the mouse click is released
        {
            int z, w;
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();//This calls the mandelbrot and draws the fractal with the current zoom
                rectangle = false;
            }
            this.Cursor = Cursors.Arrow;  
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)//This is for the zoom box allowing it to be drawn
        {
            if (rectangle)//This means that if the rectangle boolean is set to true it executes the code with in. When rectangle is set to false it will stop. This means the box will be drawn while being dragged but disappears when the mouse is released and the program zooms
            {
            g2 = e.Graphics;//New graphics
            updatePicture = new Bitmap(this.pictureBox1.Width, this.pictureBox1.Height);//A new bitmap is created
            g2.DrawImageUnscaled(updatePicture, x1, y1);//This draws the bitmap to the graphics
            e.Graphics.DrawRectangle(updatePen, zoomRect);//This draws the final rectangle with the updated coordinates
            }
        }

        

        

        
    }
}
