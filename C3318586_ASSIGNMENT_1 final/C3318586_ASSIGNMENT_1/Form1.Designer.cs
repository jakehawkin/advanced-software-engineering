﻿namespace C3318586_ASSIGNMENT_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Fractal_Start = new System.Windows.Forms.Button();
            this.Fractal_Refresh = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Save_Image_Button = new System.Windows.Forms.Button();
            this.Fractal_Exit = new System.Windows.Forms.Button();
            this.Save_State = new System.Windows.Forms.Button();
            this.Load_State = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Red_Button = new System.Windows.Forms.Button();
            this.Blue_Button = new System.Windows.Forms.Button();
            this.Green_Button = new System.Windows.Forms.Button();
            this.Pink_Button = new System.Windows.Forms.Button();
            this.Yellow_Button = new System.Windows.Forms.Button();
            this.LightBlue_Button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.pictureBox1.Location = new System.Drawing.Point(0, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(640, 480);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // Fractal_Start
            // 
            this.Fractal_Start.BackColor = System.Drawing.Color.LawnGreen;
            this.Fractal_Start.Location = new System.Drawing.Point(12, 505);
            this.Fractal_Start.Name = "Fractal_Start";
            this.Fractal_Start.Size = new System.Drawing.Size(75, 23);
            this.Fractal_Start.TabIndex = 1;
            this.Fractal_Start.Text = "Start";
            this.Fractal_Start.UseVisualStyleBackColor = false;
            this.Fractal_Start.Click += new System.EventHandler(this.Fractal_Start_Click);
            // 
            // Fractal_Refresh
            // 
            this.Fractal_Refresh.BackColor = System.Drawing.Color.Orange;
            this.Fractal_Refresh.Location = new System.Drawing.Point(12, 505);
            this.Fractal_Refresh.Name = "Fractal_Refresh";
            this.Fractal_Refresh.Size = new System.Drawing.Size(75, 23);
            this.Fractal_Refresh.TabIndex = 2;
            this.Fractal_Refresh.Text = "Restart";
            this.Fractal_Refresh.UseVisualStyleBackColor = false;
            this.Fractal_Refresh.Click += new System.EventHandler(this.Fractal_Refresh_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(119, 232);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(415, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Click Start to start the Fractal Program";
            // 
            // Save_Image_Button
            // 
            this.Save_Image_Button.BackColor = System.Drawing.Color.Yellow;
            this.Save_Image_Button.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Save_Image_Button.Location = new System.Drawing.Point(511, 505);
            this.Save_Image_Button.Name = "Save_Image_Button";
            this.Save_Image_Button.Size = new System.Drawing.Size(118, 23);
            this.Save_Image_Button.TabIndex = 5;
            this.Save_Image_Button.Text = "Save Current Image";
            this.Save_Image_Button.UseVisualStyleBackColor = false;
            this.Save_Image_Button.Click += new System.EventHandler(this.Save_Image_Button_Click);
            // 
            // Fractal_Exit
            // 
            this.Fractal_Exit.BackColor = System.Drawing.Color.Red;
            this.Fractal_Exit.Location = new System.Drawing.Point(93, 505);
            this.Fractal_Exit.Name = "Fractal_Exit";
            this.Fractal_Exit.Size = new System.Drawing.Size(75, 23);
            this.Fractal_Exit.TabIndex = 6;
            this.Fractal_Exit.Text = "Exit";
            this.Fractal_Exit.UseVisualStyleBackColor = false;
            this.Fractal_Exit.Click += new System.EventHandler(this.Fractal_Exit_Click);
            // 
            // Save_State
            // 
            this.Save_State.BackColor = System.Drawing.Color.GreenYellow;
            this.Save_State.Location = new System.Drawing.Point(431, 505);
            this.Save_State.Name = "Save_State";
            this.Save_State.Size = new System.Drawing.Size(75, 23);
            this.Save_State.TabIndex = 7;
            this.Save_State.Text = "Save State";
            this.Save_State.UseVisualStyleBackColor = false;
            this.Save_State.Click += new System.EventHandler(this.Save_State_Click);
            // 
            // Load_State
            // 
            this.Load_State.BackColor = System.Drawing.Color.GreenYellow;
            this.Load_State.Location = new System.Drawing.Point(350, 505);
            this.Load_State.Name = "Load_State";
            this.Load_State.Size = new System.Drawing.Size(75, 23);
            this.Load_State.TabIndex = 8;
            this.Load_State.Text = "Load State";
            this.Load_State.UseVisualStyleBackColor = false;
            this.Load_State.Click += new System.EventHandler(this.Load_State_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(680, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // Red_Button
            // 
            this.Red_Button.BackColor = System.Drawing.Color.Red;
            this.Red_Button.Location = new System.Drawing.Point(645, 27);
            this.Red_Button.Name = "Red_Button";
            this.Red_Button.Size = new System.Drawing.Size(31, 23);
            this.Red_Button.TabIndex = 10;
            this.Red_Button.UseVisualStyleBackColor = false;
            this.Red_Button.Click += new System.EventHandler(this.Red_Button_Click);
            // 
            // Blue_Button
            // 
            this.Blue_Button.BackColor = System.Drawing.Color.Blue;
            this.Blue_Button.Location = new System.Drawing.Point(644, 85);
            this.Blue_Button.Name = "Blue_Button";
            this.Blue_Button.Size = new System.Drawing.Size(32, 23);
            this.Blue_Button.TabIndex = 11;
            this.Blue_Button.UseVisualStyleBackColor = false;
            this.Blue_Button.Click += new System.EventHandler(this.Blue_Button_Click);
            // 
            // Green_Button
            // 
            this.Green_Button.BackColor = System.Drawing.Color.LawnGreen;
            this.Green_Button.Location = new System.Drawing.Point(644, 56);
            this.Green_Button.Name = "Green_Button";
            this.Green_Button.Size = new System.Drawing.Size(32, 23);
            this.Green_Button.TabIndex = 12;
            this.Green_Button.UseVisualStyleBackColor = false;
            this.Green_Button.Click += new System.EventHandler(this.Green_Button_Click);
            // 
            // Pink_Button
            // 
            this.Pink_Button.BackColor = System.Drawing.Color.Fuchsia;
            this.Pink_Button.Location = new System.Drawing.Point(644, 143);
            this.Pink_Button.Name = "Pink_Button";
            this.Pink_Button.Size = new System.Drawing.Size(32, 23);
            this.Pink_Button.TabIndex = 13;
            this.Pink_Button.UseVisualStyleBackColor = false;
            this.Pink_Button.Click += new System.EventHandler(this.Pink_Button_Click);
            // 
            // Yellow_Button
            // 
            this.Yellow_Button.BackColor = System.Drawing.Color.Yellow;
            this.Yellow_Button.Location = new System.Drawing.Point(644, 114);
            this.Yellow_Button.Name = "Yellow_Button";
            this.Yellow_Button.Size = new System.Drawing.Size(32, 23);
            this.Yellow_Button.TabIndex = 14;
            this.Yellow_Button.UseVisualStyleBackColor = false;
            this.Yellow_Button.Click += new System.EventHandler(this.Yellow_Button_Click);
            // 
            // LightBlue_Button
            // 
            this.LightBlue_Button.BackColor = System.Drawing.Color.Aqua;
            this.LightBlue_Button.Location = new System.Drawing.Point(644, 172);
            this.LightBlue_Button.Name = "LightBlue_Button";
            this.LightBlue_Button.Size = new System.Drawing.Size(32, 23);
            this.LightBlue_Button.TabIndex = 15;
            this.LightBlue_Button.UseVisualStyleBackColor = false;
            this.LightBlue_Button.Click += new System.EventHandler(this.LightBlue_Button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 533);
            this.Controls.Add(this.LightBlue_Button);
            this.Controls.Add(this.Yellow_Button);
            this.Controls.Add(this.Pink_Button);
            this.Controls.Add(this.Green_Button);
            this.Controls.Add(this.Blue_Button);
            this.Controls.Add(this.Red_Button);
            this.Controls.Add(this.Load_State);
            this.Controls.Add(this.Save_State);
            this.Controls.Add(this.Fractal_Exit);
            this.Controls.Add(this.Save_Image_Button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Fractal_Refresh);
            this.Controls.Add(this.Fractal_Start);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Fractal_Start;
        private System.Windows.Forms.Button Fractal_Refresh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Save_Image_Button;
        private System.Windows.Forms.Button Fractal_Exit;
        private System.Windows.Forms.Button Save_State;
        private System.Windows.Forms.Button Load_State;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Button Red_Button;
        private System.Windows.Forms.Button Blue_Button;
        private System.Windows.Forms.Button Green_Button;
        private System.Windows.Forms.Button Pink_Button;
        private System.Windows.Forms.Button Yellow_Button;
        private System.Windows.Forms.Button LightBlue_Button;
    }
}

